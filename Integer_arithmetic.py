# целочисленное деление a на b с округлением вверх
def division_with_rounding_up(a, b):
    return (a + b - 1) // b


# функция возвращает 1, если a не кратно b, иначе - 0; a>b>0
def is_not_divisible(a, b):
    # код аналогичен данному:
    # if a%b!=0:
    #     return 1
    # else:
    #     return 0
    # два числа кратны тогда и только тогда, когда
    # результат деления их друг на друга с округлением вверх равен результату от деления с округлением вниз
    # return int_division_with_rounding_up(a, b) - a // b
    return (a + b - 1) // b - a // b


# функция возвращает 1, если a меньше или равно b, иначе - 0; a,b>=0
def is_less_or_equal(a, b):
    # код аналогичен данному:
    # if a<=b:
    #     return 1
    # else:
    #     return 0
    # 0, возведенный в любую положительную степень возвращает 0
    # 1, возведенная в любую степень возвращает 1
    # любое число, возведенное в степень 0, равно 1
    # рассмотрим выражение (b//a)**(a//b), a,b>0
    # если b>a, то показатель степени равен нулю, и все выражение равно 1
    # если a>b, то основание степени равно 0, и все выражение равно 0
    # если a==b, то основание степени равно 1, и все выражение равно 1
    # чтобы избежать деления на ноль, увеличим a и b на 1
    return ((b + 1) // (a + 1)) ** ((a + 1) // (b + 1))


# функция возвращает 1, если a меньше b, иначе - 0; a,b>=0
def is_less(a, b):
    # код аналогичен данному:
    # if a<b:
    #     return 1
    # else:
    #     return 0
    # если один из сомножителей равен 0, то и произведение равно 0
    # рассмотрим выражение int_is_less_or_equal(a, b) * int_is_not_divisible(b, a)
    # если b>a, то первый сомножитель равен 0, и все выражение равно 0
    # если b==a, то второй сомножитель равен 0, и все выражение равно 0
    # если b<a, то оба сомножителя равны 1 (вторая функция теряет физический смысл, выходя за границы применимости)
    # учтем во втором сомножителе случай, когда b==a==0, увеличив a и b на 1, чтобы избежать деления на 0
    return (((b + 1) // (a + 1)) ** ((a + 1) // (b + 1))) * ((a + b + 1) // (b + 1) - (a + 1) // (b + 1))


# функция возвращает 1, если a==0, иначе - некое число
def is_equal_to_zero(a):
    # код аналогичен данному:
    # if a==0:
    #     return 1
    # else:
    #     return ...
    # любое число, возведенное в степень 0, равно 1
    return 5 ** a


# функция возвращает 1, если a==b==0, иначе - некое число
def is_both_equal_to_zero(a, b):
    # код аналогичен данному:
    # if a==b==0:
    #     return 1
    # else:
    #     return ...
    # любое число, возведенное в степень 0, равно 1
    # int_is_equal_to_zero(a+b) не работает, если a==-b!=0
    # return  int_is_equal_to_zero(a**2+b**2)
    return 5 ** (a ** 2 + b ** 2)

